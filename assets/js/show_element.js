function admSelectCheck(nameSelect, option_id, element_id)
{
    /**
    * Function to show and hide elements when selecting options from a checkbox, to be used first you must call the function
    * in your checkbox (in 'onchange =' field). The function will show an element when a option is selected
    * @author AndoreKun
    * @version 2.0
    * @since 1 jan 2021
    * @param {string} nameSelect Selection (<select>) to be read (For this value you can use 'this')
    * @param {string} option_id Option ID
    * @param {string} element_id The ID of the element you want to appear
    * @param {string} admOptionValue form/selection ID 
    */

    if(nameSelect){
        admOptionValue = document.getElementById(option_id).value;
        if(admOptionValue == nameSelect.value){
            
            document.getElementById(element_id).style.display = "block";
        }
        else{
            document.getElementById(element_id).style.display = "none";
        }
    }
    else {
        document.getElementById(element_id).style.display = "none";
    }    
    
}
